﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.txtRes = New System.Windows.Forms.TextBox()
        Me.btnNum1 = New System.Windows.Forms.Button()
        Me.btnNum2 = New System.Windows.Forms.Button()
        Me.btnNum3 = New System.Windows.Forms.Button()
        Me.btnNum4 = New System.Windows.Forms.Button()
        Me.btnNum5 = New System.Windows.Forms.Button()
        Me.btnNum6 = New System.Windows.Forms.Button()
        Me.btnNum7 = New System.Windows.Forms.Button()
        Me.btnNum8 = New System.Windows.Forms.Button()
        Me.btnNum9 = New System.Windows.Forms.Button()
        Me.btnNumPlus = New System.Windows.Forms.Button()
        Me.btnNumMines = New System.Windows.Forms.Button()
        Me.btnEquls = New System.Windows.Forms.Button()
        Me.btnClear = New System.Windows.Forms.Button()
        Me.btnNum0 = New System.Windows.Forms.Button()
        Me.txtShowA = New System.Windows.Forms.TextBox()
        Me.lblACT = New System.Windows.Forms.Label()
        Me.btnNumMulti = New System.Windows.Forms.Button()
        Me.btnNumDivision = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnClearCE = New System.Windows.Forms.Button()
        Me.btnNumDot = New System.Windows.Forms.Button()
        Me.btnNumPercent = New System.Windows.Forms.Button()
        Me.btnNumCaret = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtRes
        '
        Me.txtRes.Enabled = False
        Me.txtRes.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.txtRes.Location = New System.Drawing.Point(4, 55)
        Me.txtRes.Name = "txtRes"
        Me.txtRes.Size = New System.Drawing.Size(305, 35)
        Me.txtRes.TabIndex = 0
        Me.txtRes.Text = "0"
        '
        'btnNum1
        '
        Me.btnNum1.BackColor = System.Drawing.Color.Transparent
        Me.btnNum1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNum1.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNum1.ForeColor = System.Drawing.Color.Black
        Me.btnNum1.Location = New System.Drawing.Point(4, 153)
        Me.btnNum1.Name = "btnNum1"
        Me.btnNum1.Size = New System.Drawing.Size(61, 62)
        Me.btnNum1.TabIndex = 1
        Me.btnNum1.Text = "1"
        Me.btnNum1.UseVisualStyleBackColor = False
        '
        'btnNum2
        '
        Me.btnNum2.BackColor = System.Drawing.Color.Transparent
        Me.btnNum2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNum2.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNum2.Location = New System.Drawing.Point(71, 153)
        Me.btnNum2.Name = "btnNum2"
        Me.btnNum2.Size = New System.Drawing.Size(61, 62)
        Me.btnNum2.TabIndex = 2
        Me.btnNum2.Text = "2"
        Me.btnNum2.UseVisualStyleBackColor = False
        '
        'btnNum3
        '
        Me.btnNum3.BackColor = System.Drawing.Color.Transparent
        Me.btnNum3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNum3.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNum3.Location = New System.Drawing.Point(137, 153)
        Me.btnNum3.Name = "btnNum3"
        Me.btnNum3.Size = New System.Drawing.Size(61, 62)
        Me.btnNum3.TabIndex = 3
        Me.btnNum3.Text = "3"
        Me.btnNum3.UseVisualStyleBackColor = False
        '
        'btnNum4
        '
        Me.btnNum4.BackColor = System.Drawing.Color.Transparent
        Me.btnNum4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNum4.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNum4.Location = New System.Drawing.Point(137, 221)
        Me.btnNum4.Name = "btnNum4"
        Me.btnNum4.Size = New System.Drawing.Size(61, 62)
        Me.btnNum4.TabIndex = 4
        Me.btnNum4.Text = "4"
        Me.btnNum4.UseVisualStyleBackColor = False
        '
        'btnNum5
        '
        Me.btnNum5.BackColor = System.Drawing.Color.Transparent
        Me.btnNum5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNum5.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNum5.Location = New System.Drawing.Point(71, 221)
        Me.btnNum5.Name = "btnNum5"
        Me.btnNum5.Size = New System.Drawing.Size(61, 62)
        Me.btnNum5.TabIndex = 5
        Me.btnNum5.Text = "5"
        Me.btnNum5.UseVisualStyleBackColor = False
        '
        'btnNum6
        '
        Me.btnNum6.BackColor = System.Drawing.Color.Transparent
        Me.btnNum6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNum6.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNum6.Location = New System.Drawing.Point(4, 221)
        Me.btnNum6.Name = "btnNum6"
        Me.btnNum6.Size = New System.Drawing.Size(61, 62)
        Me.btnNum6.TabIndex = 6
        Me.btnNum6.Text = "6"
        Me.btnNum6.UseVisualStyleBackColor = False
        '
        'btnNum7
        '
        Me.btnNum7.BackColor = System.Drawing.Color.Transparent
        Me.btnNum7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNum7.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNum7.Location = New System.Drawing.Point(4, 289)
        Me.btnNum7.Name = "btnNum7"
        Me.btnNum7.Size = New System.Drawing.Size(61, 62)
        Me.btnNum7.TabIndex = 7
        Me.btnNum7.Text = "7"
        Me.btnNum7.UseVisualStyleBackColor = False
        '
        'btnNum8
        '
        Me.btnNum8.BackColor = System.Drawing.Color.Transparent
        Me.btnNum8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNum8.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNum8.Location = New System.Drawing.Point(71, 289)
        Me.btnNum8.Name = "btnNum8"
        Me.btnNum8.Size = New System.Drawing.Size(61, 62)
        Me.btnNum8.TabIndex = 8
        Me.btnNum8.Text = "8"
        Me.btnNum8.UseVisualStyleBackColor = False
        '
        'btnNum9
        '
        Me.btnNum9.BackColor = System.Drawing.Color.Transparent
        Me.btnNum9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNum9.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNum9.Location = New System.Drawing.Point(137, 289)
        Me.btnNum9.Name = "btnNum9"
        Me.btnNum9.Size = New System.Drawing.Size(61, 62)
        Me.btnNum9.TabIndex = 9
        Me.btnNum9.Text = "9"
        Me.btnNum9.UseVisualStyleBackColor = False
        '
        'btnNumPlus
        '
        Me.btnNumPlus.BackColor = System.Drawing.Color.Transparent
        Me.btnNumPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNumPlus.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNumPlus.Location = New System.Drawing.Point(204, 221)
        Me.btnNumPlus.Name = "btnNumPlus"
        Me.btnNumPlus.Size = New System.Drawing.Size(105, 62)
        Me.btnNumPlus.TabIndex = 10
        Me.btnNumPlus.Text = "+"
        Me.btnNumPlus.UseVisualStyleBackColor = False
        '
        'btnNumMines
        '
        Me.btnNumMines.BackColor = System.Drawing.Color.Transparent
        Me.btnNumMines.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNumMines.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNumMines.Location = New System.Drawing.Point(205, 289)
        Me.btnNumMines.Name = "btnNumMines"
        Me.btnNumMines.Size = New System.Drawing.Size(103, 62)
        Me.btnNumMines.TabIndex = 11
        Me.btnNumMines.Text = "-"
        Me.btnNumMines.UseVisualStyleBackColor = False
        '
        'btnEquls
        '
        Me.btnEquls.BackColor = System.Drawing.Color.Transparent
        Me.btnEquls.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEquls.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnEquls.Location = New System.Drawing.Point(205, 153)
        Me.btnEquls.Name = "btnEquls"
        Me.btnEquls.Size = New System.Drawing.Size(104, 62)
        Me.btnEquls.TabIndex = 12
        Me.btnEquls.Text = "="
        Me.btnEquls.UseVisualStyleBackColor = False
        '
        'btnClear
        '
        Me.btnClear.BackColor = System.Drawing.Color.Transparent
        Me.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClear.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnClear.Location = New System.Drawing.Point(71, 96)
        Me.btnClear.Name = "btnClear"
        Me.btnClear.Size = New System.Drawing.Size(183, 51)
        Me.btnClear.TabIndex = 13
        Me.btnClear.Text = "C"
        Me.btnClear.UseVisualStyleBackColor = False
        '
        'btnNum0
        '
        Me.btnNum0.BackColor = System.Drawing.Color.Transparent
        Me.btnNum0.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNum0.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNum0.Location = New System.Drawing.Point(71, 357)
        Me.btnNum0.Name = "btnNum0"
        Me.btnNum0.Size = New System.Drawing.Size(61, 62)
        Me.btnNum0.TabIndex = 14
        Me.btnNum0.Text = "0"
        Me.btnNum0.UseVisualStyleBackColor = False
        '
        'txtShowA
        '
        Me.txtShowA.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me.txtShowA.Enabled = False
        Me.txtShowA.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.txtShowA.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtShowA.Location = New System.Drawing.Point(4, 6)
        Me.txtShowA.Name = "txtShowA"
        Me.txtShowA.Size = New System.Drawing.Size(250, 26)
        Me.txtShowA.TabIndex = 15
        '
        'lblACT
        '
        Me.lblACT.AutoSize = True
        Me.lblACT.BackColor = System.Drawing.Color.Transparent
        Me.lblACT.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.lblACT.ForeColor = System.Drawing.Color.Black
        Me.lblACT.Location = New System.Drawing.Point(59, 37)
        Me.lblACT.Name = "lblACT"
        Me.lblACT.Size = New System.Drawing.Size(0, 15)
        Me.lblACT.TabIndex = 16
        '
        'btnNumMulti
        '
        Me.btnNumMulti.BackColor = System.Drawing.Color.Transparent
        Me.btnNumMulti.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNumMulti.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNumMulti.Location = New System.Drawing.Point(204, 357)
        Me.btnNumMulti.Name = "btnNumMulti"
        Me.btnNumMulti.Size = New System.Drawing.Size(49, 62)
        Me.btnNumMulti.TabIndex = 17
        Me.btnNumMulti.Text = "×"
        Me.btnNumMulti.UseVisualStyleBackColor = False
        '
        'btnNumDivision
        '
        Me.btnNumDivision.BackColor = System.Drawing.Color.Transparent
        Me.btnNumDivision.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNumDivision.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNumDivision.Location = New System.Drawing.Point(259, 357)
        Me.btnNumDivision.Name = "btnNumDivision"
        Me.btnNumDivision.Size = New System.Drawing.Size(49, 62)
        Me.btnNumDivision.TabIndex = 18
        Me.btnNumDivision.Text = "÷"
        Me.btnNumDivision.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label1.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(150, 219)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(0, 19)
        Me.Label1.TabIndex = 19
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label2.Location = New System.Drawing.Point(6, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 15)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Action:"
        '
        'btnClearCE
        '
        Me.btnClearCE.BackColor = System.Drawing.Color.Transparent
        Me.btnClearCE.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClearCE.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnClearCE.Location = New System.Drawing.Point(4, 96)
        Me.btnClearCE.Name = "btnClearCE"
        Me.btnClearCE.Size = New System.Drawing.Size(61, 51)
        Me.btnClearCE.TabIndex = 21
        Me.btnClearCE.Text = "CE"
        Me.btnClearCE.UseVisualStyleBackColor = False
        '
        'btnNumDot
        '
        Me.btnNumDot.BackColor = System.Drawing.Color.Transparent
        Me.btnNumDot.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNumDot.Font = New System.Drawing.Font("Times New Roman", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNumDot.Location = New System.Drawing.Point(137, 357)
        Me.btnNumDot.Name = "btnNumDot"
        Me.btnNumDot.Size = New System.Drawing.Size(61, 62)
        Me.btnNumDot.TabIndex = 22
        Me.btnNumDot.Text = "."
        Me.btnNumDot.UseVisualStyleBackColor = False
        '
        'btnNumPercent
        '
        Me.btnNumPercent.BackColor = System.Drawing.Color.Transparent
        Me.btnNumPercent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNumPercent.Font = New System.Drawing.Font("Times New Roman", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNumPercent.Location = New System.Drawing.Point(4, 357)
        Me.btnNumPercent.Name = "btnNumPercent"
        Me.btnNumPercent.Size = New System.Drawing.Size(61, 62)
        Me.btnNumPercent.TabIndex = 23
        Me.btnNumPercent.Text = "%"
        Me.btnNumPercent.UseVisualStyleBackColor = False
        '
        'btnNumCaret
        '
        Me.btnNumCaret.BackColor = System.Drawing.Color.Transparent
        Me.btnNumCaret.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNumCaret.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnNumCaret.Location = New System.Drawing.Point(260, 96)
        Me.btnNumCaret.Name = "btnNumCaret"
        Me.btnNumCaret.Size = New System.Drawing.Size(49, 51)
        Me.btnNumCaret.TabIndex = 24
        Me.btnNumCaret.Text = "^2"
        Me.btnNumCaret.UseVisualStyleBackColor = False
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.Transparent
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnExit.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.btnExit.Location = New System.Drawing.Point(260, 6)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(49, 26)
        Me.btnExit.TabIndex = 25
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gainsboro
        Me.BackgroundImage = Global.SimpleCalc.My.Resources.Resources.image49s
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(313, 425)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnNumCaret)
        Me.Controls.Add(Me.btnNumPercent)
        Me.Controls.Add(Me.btnNumDot)
        Me.Controls.Add(Me.btnClearCE)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnNumDivision)
        Me.Controls.Add(Me.btnNumMulti)
        Me.Controls.Add(Me.lblACT)
        Me.Controls.Add(Me.txtShowA)
        Me.Controls.Add(Me.btnNum0)
        Me.Controls.Add(Me.btnClear)
        Me.Controls.Add(Me.btnEquls)
        Me.Controls.Add(Me.btnNumMines)
        Me.Controls.Add(Me.btnNumPlus)
        Me.Controls.Add(Me.btnNum9)
        Me.Controls.Add(Me.btnNum8)
        Me.Controls.Add(Me.btnNum7)
        Me.Controls.Add(Me.btnNum6)
        Me.Controls.Add(Me.btnNum5)
        Me.Controls.Add(Me.btnNum4)
        Me.Controls.Add(Me.btnNum3)
        Me.Controls.Add(Me.btnNum2)
        Me.Controls.Add(Me.btnNum1)
        Me.Controls.Add(Me.txtRes)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Fast Calculator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtRes As TextBox
    Friend WithEvents btnNum1 As Button
    Friend WithEvents btnNum2 As Button
    Friend WithEvents btnNum3 As Button
    Friend WithEvents btnNum4 As Button
    Friend WithEvents btnNum5 As Button
    Friend WithEvents btnNum6 As Button
    Friend WithEvents btnNum7 As Button
    Friend WithEvents btnNum8 As Button
    Friend WithEvents btnNum9 As Button
    Friend WithEvents btnNumPlus As Button
    Friend WithEvents btnNumMines As Button
    Friend WithEvents btnEquls As Button
    Friend WithEvents btnClear As Button
    Friend WithEvents btnNum0 As Button
    Friend WithEvents txtShowA As TextBox
    Friend WithEvents lblACT As Label
    Friend WithEvents btnNumMulti As Button
    Friend WithEvents btnNumDivision As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnClearCE As Button
    Friend WithEvents btnNumDot As Button
    Friend WithEvents btnNumPercent As Button
    Friend WithEvents btnNumCaret As Button
    Friend WithEvents btnExit As Button
End Class
